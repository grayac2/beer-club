## Beer Club

**Team Members:** Adam Gray, Josh Harless, Tyler Jinks, Kendal Nelson, Logan Dykes

#### About
This project was completed for a Software Engineering course at East Tennessee State University. 
The goal of this project was to build a mobile-responsive, social media web application for beer enthusiasts.

#### Languages
* PHP
* Laravel